use chrono::{TimeZone, Utc};
use rusqlite::types::ValueRef;
use rusqlite::{params, Connection};
use std::collections::BTreeMap;
use std::fs;
use std::fs::File;
use std::io::Write;
use std::path::Path;

static DATABASE: &'static str = "./biboumi.sqlite";
static OUTDIR: &'static str = "./out";

fn main() {
    if !Path::new(DATABASE).exists() {
        panic!("{} does not exist", DATABASE);
    }
    let conn = Connection::open(DATABASE).unwrap();
    // get a list of all buffers
    let mut stmt = conn
        .prepare("SELECT DISTINCT ircchanname_, ircservername_ FROM muclogline_")
        .unwrap();
    let rows = stmt
        .query_map([], |row| {
            Ok((row.get::<_, String>(0)?, row.get::<_, String>(1)?))
        })
        .unwrap();
    let buffers = rows.collect::<Result<Vec<_>, _>>().unwrap();
    eprintln!("found {} buffers in database", buffers.len());

    // iterate through each buffer and print it out
    for (chan, server) in buffers {
        let path = format!("{}/{}/{}", OUTDIR, server, chan);
        eprint!("writing {}...", path);
        fs::create_dir_all(&path).unwrap();
        // get all messages for this buffer
        let mut stmt = conn.prepare("SELECT date_, nick_, body_ FROM muclogline_ WHERE ircchanname_ = $1 AND ircservername_ = $2 ORDER BY date_ ASC")
            .unwrap();
        let rows = stmt
            .query_map(params![&chan, &server], |row| {
                let text = match row.get_ref(2)? {
                    ValueRef::Text(txt) => String::from_utf8_lossy(txt).into_owned(),
                    x => panic!("{:?} is not text", x),
                };
                Ok((
                    Utc.timestamp(row.get(0)?, 0),
                    row.get::<_, String>(1)?,
                    text,
                ))
            })
            .unwrap();
        let mut lines_by_date = BTreeMap::new();
        for row in rows {
            let (ts, nick, body) = row.unwrap();
            let entry = lines_by_date.entry(ts.date()).or_insert(vec![]);
            entry.push((ts, nick, body));
        }
        let lines_by_date = lines_by_date.into_iter().collect::<Vec<_>>();
        for (idx, (date, lines)) in lines_by_date.iter().enumerate() {
            let path = format!("{}/{}.txt", path, date.format("%Y-%m-%d"));
            let mut file = File::create(path).unwrap();
            // write a header invoking a MediaWiki template
            let prevdate = if idx != 0 {
                format!("|prevdate={}", lines_by_date[idx - 1].0.format("%Y-%m-%d"))
            } else {
                "".into()
            };
            let nextdate = if idx != lines_by_date.len() - 1 {
                format!("|nextdate={}", lines_by_date[idx + 1].0.format("%Y-%m-%d"))
            } else {
                "".into()
            };
            file.write_all(
                format!(
                    "{{{{logpage|source=biboumi|server={}|channel={}|date={}{}{}}}}}<pre>\n",
                    server,
                    chan,
                    date.format("%Y-%m-%d"),
                    nextdate,
                    prevdate
                )
                .as_bytes(),
            )
            .unwrap();
            // write the actual log lines
            for (ts, nick, body) in lines {
                let ts = format!("{}", ts.format("%H:%M:%S"));
                let line = if nick == "" {
                    format!("{} {}\n", ts, body)
                } else if body.starts_with("/me ") {
                    format!("{} * {} {}\n", ts, nick, &body[4..])
                } else {
                    format!("{} <{}> {}\n", ts, nick, body)
                };
                file.write_all(line.as_bytes()).unwrap();
            }
            file.write_all("</pre>\n".as_bytes()).unwrap();
        }
        eprintln!("wrote {} files", lines_by_date.len());
    }
}
