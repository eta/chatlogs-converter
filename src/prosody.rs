use chrono::{TimeZone, Utc};
use minidom::{Element, NSChoice};
use rusqlite::types::ValueRef;
use rusqlite::{params, Connection};
use std::collections::{BTreeMap, HashMap};
use std::fs;
use std::fs::File;
use std::io::Write;
use std::path::Path;

static DATABASE: &'static str = "./prosody.sqlite";
// /usr/libexec/evolution-data-server/addressbook-export --format=csv | csvgrep -c mobile_phone -r '$^' -i | csvcut -c full_name,mobile_phone
// then trim to remove header row
// first field is name, second field is phone number
static PHONE_NUMBERS: &'static str = "./contacts.csv";
static OUTDIR: &'static str = "./prosody";

#[derive(Debug, Hash, PartialEq, Eq, Clone)]
enum Counterparty {
    Irc { party: String, network: String },
    WhatsApp { phone: String, name: Option<String> },
    Native(String),
}

fn extract_body(elem: &Element) -> Option<(String, String)> {
    if let Some(message) = elem.get_child("message", NSChoice::Any) {
        let from = message.attr("from");
        if from.is_none() {
            return None;
        }
        if message.attr("type").map(|x| x == "chat").unwrap_or(true) {
            if let Some(body) = message.get_child("body", NSChoice::Any) {
                // don't return OMEMO-encrypted messages
                if message.get_child("encrypted", NSChoice::Any).is_none() {
                    return Some((from.unwrap().to_owned(), body.text()));
                }
            }
        }
    }
    None
}

fn main() {
    if !Path::new(DATABASE).exists() {
        panic!("{} does not exist", DATABASE);
    }
    if !Path::new(PHONE_NUMBERS).exists() {
        panic!("{} does not exist", PHONE_NUMBERS);
    }
    // load in mapping of phone number -> realname
    let mut address_book = HashMap::new();
    let mut address_book_rev = HashMap::new();
    let mut reader = csv::Reader::from_path(PHONE_NUMBERS).unwrap();
    for result in reader.records() {
        let record = result.unwrap();
        let name = record.get(0).expect("no name in entry");
        // FIXME(eta): this is stupid :p
        let phone_number = record
            .get(1)
            .expect("no phone number in entry")
            .replace("-", "")
            .replace("(", "")
            .replace(")", "")
            .replace(" ", "");
        let phone_number = if phone_number.starts_with("0") {
            format!("+44{}", &phone_number[1..])
        } else {
            phone_number
        };
        if let Some(number) = address_book_rev.get(name) {
            if number != &phone_number {
                eprintln!(
                    "{} is already {} (tried to add {})",
                    name, number, phone_number
                );
                if !address_book.contains_key(&phone_number) {
                    address_book
                        .insert(phone_number.clone(), format!("{} ({})", name, phone_number));
                    continue;
                }
            }
        } else {
            address_book_rev.insert(name.to_owned(), phone_number.clone());
        }
        if !address_book.contains_key(&phone_number) {
            address_book.insert(phone_number.clone(), name.to_owned());
        }
    }
    eprintln!("loaded {} address book entries", address_book.len());
    let conn = Connection::open(DATABASE).unwrap();

    // get a list of all counterparties
    let mut stmt = conn
        .prepare("SELECT DISTINCT with FROM prosodyarchive WHERE user = 'eta'")
        .unwrap();
    let rows = stmt
        .query_map([], |row| Ok(row.get::<_, String>(0)?))
        .unwrap();
    let buffers = rows.collect::<Result<Vec<_>, _>>().unwrap();
    eprintln!("found {} `with`s in database", buffers.len());
    let mut wa_found = 0;
    let mut wa_total = 0;
    let mut irc_total = 0;
    let counterparties_list = buffers
        .into_iter()
        .filter_map(|jid| {
            if jid.ends_with("@irc.eta.st") || jid.ends_with("@biboumi.theta.eu.org") {
                if jid.starts_with("#") {
                    None
                } else {
                    let perc = jid.find("%")?;
                    let at = jid.find("@")?;
                    let user = &jid[0..perc];
                    let network = &jid[(perc + 1)..at];
                    irc_total += 1;
                    Some((
                        Counterparty::Irc {
                            party: user.to_string(),
                            network: network.to_string(),
                        },
                        jid.clone(),
                    ))
                }
            } else if jid.ends_with("@whatsapp.theta.eu.org") || jid.ends_with("@wa.eta.st") {
                if !jid.starts_with("u") {
                    None
                } else {
                    let at = jid.find("@")?;
                    let phone = format!("+{}", &jid[1..at]);
                    wa_total += 1;
                    let name = address_book.get(&phone).map(|x| x.to_owned());
                    if name.is_some() {
                        wa_found += 1;
                    } else {
                        eprintln!("dunno who {} is", phone);
                    }
                    Some((Counterparty::WhatsApp { phone, name }, jid.clone()))
                }
            } else if jid.ends_with("@conf.theta.eu.org") {
                None
            } else {
                Some((Counterparty::Native(jid.clone()), jid))
            }
        })
        .collect::<Vec<_>>();
    eprintln!(
        "filtered that down to {} ({} irc, {} in address book, {} WA)",
        counterparties_list.len(),
        irc_total,
        wa_found,
        wa_total
    );
    let mut counterparties = HashMap::new();
    for (mut counterparty, jid) in counterparties_list {
        counterparties
            .entry(counterparty)
            .or_insert(vec![])
            .push(jid);
    }
    eprintln!("found {} counterparties", counterparties.len());
    for (counterparty, jids) in counterparties {
        let path = match counterparty.clone() {
            Counterparty::Irc { party, network } => format!("{}/{}/{}", OUTDIR, network, party),
            Counterparty::WhatsApp { phone, name } => {
                format!("{}/whatsapp/{}", OUTDIR, name.as_ref().unwrap_or(&phone))
            }
            Counterparty::Native(jid) => format!("{}/{}", OUTDIR, jid),
        };
        eprint!("processing {}...", path);
        let jids_text = jids
            .iter()
            .map(|x| format!("\"with\" = \'{}\'", x))
            .collect::<Vec<String>>()
            .join(" OR ");
        let stmt_text = format!(
            "SELECT \"when\", value FROM prosodyarchive WHERE (host = 'eta.st' OR host = 'theta.eu.org') AND user = 'eta' AND store = 'archive' AND {}{}{} ORDER BY \"when\" ASC",
            if jids.len() > 1 { "(" } else { " " },
            jids_text,
            if jids.len() > 1 { ")" } else { " " }
        );
        // FIXME(eta): this is a SQLi problem but it's hard to do it any other way, so lol
        let mut stmt = conn.prepare(&stmt_text).unwrap();
        let rows = stmt
            .query_map(params![], |row| {
                Ok((Utc.timestamp(row.get(0)?, 0), row.get::<_, String>(1)?))
            })
            .unwrap();
        let mut lines_by_date = BTreeMap::new();
        for result in rows {
            let (ts, xml) = result.unwrap();
            let xml_haxx = format!("<root xmlns=\"lol\">{}</root>", xml);
            let xml: Element = match xml_haxx.parse() {
                Ok(x) => x,
                Err(e) => {
                    eprintln!("failed to parse XML: {}", e);
                    continue;
                }
            };
            if let Some((from, body)) = extract_body(&xml) {
                let entry = lines_by_date.entry(ts.date()).or_insert(vec![]);
                entry.push((ts, from, body));
            }
        }
        let lines_by_date = lines_by_date.into_iter().collect::<Vec<_>>();
        if !lines_by_date.is_empty() {
            fs::create_dir_all(&path).unwrap();
        }
        for (idx, (date, lines)) in lines_by_date.iter().enumerate() {
            let path = format!("{}/{}.txt", path, date.format("%Y-%m-%d"));
            let mut file = File::create(path).unwrap();
            // write a header invoking a MediaWiki template
            let prevdate = if idx != 0 {
                format!("|prevdate={}", lines_by_date[idx - 1].0.format("%Y-%m-%d"))
            } else {
                "".into()
            };
            let nextdate = if idx != lines_by_date.len() - 1 {
                format!("|nextdate={}", lines_by_date[idx + 1].0.format("%Y-%m-%d"))
            } else {
                "".into()
            };
            let (attrs, cnick) = match counterparty {
                Counterparty::Irc {
                    ref party,
                    ref network,
                } => (format!("server={}|user={}", network, party), party as &str),
                Counterparty::WhatsApp {
                    ref phone,
                    ref name,
                } => (
                    format!(
                        "server=whatsapp|user={}|phone={}",
                        name.as_ref().unwrap_or(phone),
                        phone
                    ),
                    name.as_ref().map(|x| x as &str).unwrap_or("other"),
                ),
                Counterparty::Native(ref jid) => {
                    let at = jid.find("@").unwrap_or(jid.len());
                    (format!("server=xmpp|user={}", jid), &jid[0..at])
                }
            };
            file.write_all(
                format!(
                    "{{{{logpage|source=xmpp|{}|date={}{}{}}}}}<pre>\n",
                    attrs,
                    date.format("%Y-%m-%d"),
                    nextdate,
                    prevdate
                )
                .as_bytes(),
            )
            .unwrap();
            // write the actual log lines
            for (ts, nick, body) in lines {
                let ts = format!("{}", ts.format("%H:%M:%S"));
                let nick = if nick.starts_with("eta@") {
                    "eta"
                } else {
                    cnick
                };
                let line = if body.starts_with("/me ") {
                    format!("{} * {} {}\n", ts, nick, &body[4..])
                } else {
                    format!("{} <{}> {}\n", ts, nick, body)
                };
                file.write_all(line.as_bytes()).unwrap();
            }
            file.write_all("</pre>\n".as_bytes()).unwrap();
        }
        eprintln!("wrote {} files", lines_by_date.len());
    }
}
